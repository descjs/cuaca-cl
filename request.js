$(document).ready(() => {
  ajx(
    `http://api.openweathermap.org/data/2.5/weather?q=indonesia&appid=60f1b5a2750a13c0814ea0efc541a4f9`
  )
    .then((data) => {
      console.log(data);
      $("#temp").html(data.main.temp + "&deg;");
      $("#angin").html(data.wind.speed);
      $("#cuaca").html(data.weather[0].main);
    })
    .fail(() => {
      console.log("error");
    });
  $(".input-item").keyup(function () {
    var val = $(this).val();
    if (val == "") {
    } else {
      var url = `http://api.openweathermap.org/data/2.5/weather?q=${val}&appid=60f1b5a2750a13c0814ea0efc541a4f9`;
      ajx(url)
        .then((data) => {
          console.log(data);
          $("#temp").html(data.main.temp + "&deg;");
          $("#angin").html(data.wind.speed);
          $("#cuaca").html(data.weather[0].main);
        })
        .fail(() => {});
    }
  });
  function ajx(url) {
    return $.ajax({
      url: url,
      method: "get",
    });
  }
});
